﻿using ELEKS_REST.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Context
{
    public class MainContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<UserProduct> UserProducts { get; set; }

        public MainContext(DbContextOptions<MainContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Server = localhost\\SQLEXPRESS; Database = restful; Trusted_Connection = true; MultipleActiveResultSets = true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Timestamps (default datetime value)
            modelBuilder.Entity<User>().Property(c => c.Created_At).HasDefaultValueSql("now()");
            modelBuilder.Entity<User>().Property(c => c.Updated_At).HasDefaultValueSql("now()");

            modelBuilder.Entity<Category>().Property(c => c.Created_At).HasDefaultValueSql("now()");
            modelBuilder.Entity<Category>().Property(c => c.Updated_At).HasDefaultValueSql("now()");

            modelBuilder.Entity<Product>().Property(c => c.Created_At).HasDefaultValueSql("now()");
            modelBuilder.Entity<Product>().Property(c => c.Updated_At).HasDefaultValueSql("now()");

            modelBuilder.Entity<UserProduct>().Property(c => c.Created_At).HasDefaultValueSql("now()");
            modelBuilder.Entity<UserProduct>().Property(c => c.Updated_At).HasDefaultValueSql("now()");


            // Seeds
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Email = "vod.ukr@gmail.com", Password = "test123", LastName = "Dudka", FirstName = "Vladyslav", MidName = "Oleksandrovich", PhoneNumber = "+380507377789", Role = "admin" },
                new User { Id = 2, Email = "example@mail.com", Password = "cust123456", LastName = "Taranov", FirstName = "Igor", MidName = "Oleksandrovich", PhoneNumber = "+380739999999", Role = "customer" }
            );
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "Category 1" },
                new Category { Id = 2, Name = "Category 2" },
                new Category { Id = 3, Name = "Category 3" },
                new Category { Id = 4, Name = "Category 4" }
            );

            Random rand = new Random();

            for (int i = 1; i < 100; i++)
                modelBuilder.Entity<Product>().HasData(
                new { Id = i, Name = "Product " + i, Price = Convert.ToSingle(rand.Next(10000)) / 100, UnitsInStock = 50, CategoryId = rand.Next(1, 4), Updated_At = DateTime.Now, Created_At = DateTime.Now, }
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}
