﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ELEKS_REST
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> Expand<T>(this IQueryable<T> query, string path) where T : class
        {
            return query.Include(path);
        }

        public static IQueryable<T> Expand<T, TProperty>(this IQueryable<T> query, Expression<Func<T, TProperty>> path) where T : class
        {
            return query.Include(path);
        }
    }
}
