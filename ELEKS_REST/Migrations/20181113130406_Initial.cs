﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ELEKS_REST.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(type: "varchar(60)", nullable: false),
                    Created_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()"),
                    Updated_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Email = table.Column<string>(type: "varchar(50)", nullable: false),
                    Password = table.Column<string>(type: "varchar(1000)", nullable: false),
                    LastName = table.Column<string>(type: "varchar(40)", nullable: true),
                    FirstName = table.Column<string>(type: "varchar(40)", nullable: true),
                    MidName = table.Column<string>(type: "varchar(40)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "varchar(13)", nullable: true),
                    Role = table.Column<string>(nullable: true),
                    Token = table.Column<string>(type: "varchar(1000)", nullable: true),
                    Created_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()"),
                    Updated_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(type: "varchar(255)", nullable: false),
                    Price = table.Column<float>(nullable: false),
                    UnitsInStock = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    Created_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()"),
                    Updated_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserProducts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    UserId = table.Column<int>(nullable: true),
                    ProductId = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Created_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()"),
                    Updated_At = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserProducts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Created_At", "Name", "Updated_At" },
                values: new object[,]
                {
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Category 1", new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Category 2", new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Category 3", new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Category 4", new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Created_At", "Email", "FirstName", "LastName", "MidName", "Password", "PhoneNumber", "Role", "Token", "Updated_At" },
                values: new object[,]
                {
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "vod.ukr@gmail.com", "Vladyslav", "Dudka", "Oleksandrovich", "test123", "+380507377789", "admin", "", new DateTime(2018, 11, 13, 15, 4, 5, 729, DateTimeKind.Local) },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "example@mail.com", "Igor", "Taranov", "Oleksandrovich", "cust123456", "+380739999999", "customer", "", new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Created_At", "Name", "Price", "UnitsInStock", "Updated_At" },
                values: new object[,]
                {
                    { 9, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 9", 34.39f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 14, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 14", 61.32f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 13, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 13", 99.75f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 7, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 7", 23.09f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 5, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 5", 54.59f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 3, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 3", 26.39f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 2, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 2", 49.12f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 1, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 1", 62.66f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 91, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 91", 94f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 89, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 89", 20.02f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 88, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 88", 51.78f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 79, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 79", 20.08f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 75, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 75", 46.53f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 74, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 74", 56.42f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 71, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 71", 9.85f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 67, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 67", 36.14f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 63, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 63", 98.56f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 60, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 60", 14.55f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 59, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 59", 71.87f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 55, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 55", 80.77f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 54, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 54", 88.8f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 52, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 52", 87.05f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 35, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 35", 28.21f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 51, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 51", 6.69f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 36, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 36", 45.28f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 38, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 38", 77.09f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 96, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 96", 93.1f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 95, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 95", 12.27f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 93, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 93", 32.69f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 87, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 87", 57.47f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 86, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 86", 17.15f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 85, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 85", 28.68f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 84, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 84", 68.67f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 81, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 81", 72.39f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 78, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 78", 39.56f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 73, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 73", 12.08f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 72, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 72", 2.72f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 69, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 69", 58.99f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 66, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 66", 1.49f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 65, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 65", 59.18f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 58, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 58", 79.91f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 53, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 53", 82.94f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 46, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 46", 81.43f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 44, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 44", 7.68f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 43, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 43", 97.79f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 42, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 42", 48.29f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 40, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 40", 86.56f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 37, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 37", 28.95f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 97, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 97", 26.02f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 50, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 50", 85.93f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 41, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 41", 64.98f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 62, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 62", 31.71f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 61, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 61", 95.16f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 57, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 57", 38.42f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 56, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 56", 93.16f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 48, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 48", 18.6f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 47, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 47", 68.49f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 45, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 45", 32.99f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 39, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 39", 46.76f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 33, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 33", 60.3f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 32, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 32", 43.05f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 31, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 31", 22.21f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 29, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 29", 26.9f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 26, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 26", 44.44f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 25, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 25", 40.41f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 24, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 24", 16.51f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 23, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 23", 37.57f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 21, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 21", 28.31f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 18, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 18", 45.17f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 12, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 12", 68.32f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 11, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 11", 80.92f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 10, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 10", 18.6f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 64, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 64", 1.77f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 49, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 49", 68.18f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 68, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 68", 80.12f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 76, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 76", 36.26f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 34, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 34", 6.24f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 30, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 30", 95.62f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 28, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 28", 25.38f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 27, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 27", 40.18f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 22, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 22", 75.96f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 20, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 20", 71.34f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 19, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 19", 19.83f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 17, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 17", 41.37f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 16, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 16", 16.42f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 15, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 15", 12.48f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 8, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 8", 88.33f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 6, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 6", 50.71f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 4, 2, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 4", 8.88f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 98, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 98", 51.22f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 94, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 94", 34.11f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 92, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 92", 55.98f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 90, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 90", 10.88f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 83, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 83", 54.2f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 82, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 82", 3.2f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 80, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 80", 61.85f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 77, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 77", 49.04f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 70, 1, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 70", 1.31f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                    { 99, 3, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), "Product 99", 42.07f, 50, new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProducts_ProductId",
                table: "UserProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProducts_UserId",
                table: "UserProducts",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserProducts");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
