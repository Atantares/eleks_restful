﻿// <auto-generated />
using System;
using ELEKS_REST.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ELEKS_REST.Migrations
{
    [DbContext(typeof(MainContext))]
    partial class MainContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("ELEKS_REST.Model.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created_At")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(60)");

                    b.Property<DateTime>("Updated_At")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("Categories");

                    b.HasData(
                        new { Id = 1, Created_At = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Name = "Category 1", Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) },
                        new { Id = 2, Created_At = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Name = "Category 2", Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) },
                        new { Id = 3, Created_At = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Name = "Category 3", Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) },
                        new { Id = 4, Created_At = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Name = "Category 4", Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) }
                    );
                });

            modelBuilder.Entity("ELEKS_REST.Model.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<DateTime>("Created_At")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(255)");

                    b.Property<float>("Price");

                    b.Property<int>("UnitsInStock");

                    b.Property<DateTime>("Updated_At")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.ToTable("Products");

                    b.HasData(
                        new { Id = 1, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 1", Price = 62.66f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 2, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 2", Price = 49.12f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 3, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 3", Price = 26.39f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 4, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 4", Price = 8.88f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 5, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 5", Price = 54.59f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 6, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 6", Price = 50.71f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 7, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 7", Price = 23.09f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 8, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 8", Price = 88.33f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 9, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 9", Price = 34.39f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 10, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 10", Price = 18.6f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 11, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 11", Price = 80.92f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 12, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 12", Price = 68.32f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 13, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 13", Price = 99.75f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 14, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 14", Price = 61.32f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 15, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 15", Price = 12.48f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 16, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 16", Price = 16.42f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 17, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 17", Price = 41.37f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 18, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 18", Price = 45.17f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 19, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 19", Price = 19.83f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 20, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 20", Price = 71.34f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 21, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 21", Price = 28.31f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 22, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 22", Price = 75.96f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 23, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 23", Price = 37.57f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 24, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 24", Price = 16.51f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 25, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 25", Price = 40.41f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 26, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 26", Price = 44.44f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 27, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 27", Price = 40.18f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 28, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 28", Price = 25.38f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 29, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 29", Price = 26.9f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 30, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 30", Price = 95.62f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 31, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 31", Price = 22.21f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 32, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 32", Price = 43.05f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 33, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 33", Price = 60.3f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 34, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 34", Price = 6.24f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 35, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 35", Price = 28.21f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 36, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 36", Price = 45.28f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 37, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 37", Price = 28.95f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 38, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 38", Price = 77.09f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 39, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 39", Price = 46.76f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 40, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 40", Price = 86.56f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 41, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 41", Price = 64.98f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 42, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 42", Price = 48.29f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 43, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 43", Price = 97.79f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 44, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 44", Price = 7.68f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 45, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 45", Price = 32.99f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 46, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 46", Price = 81.43f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 47, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 47", Price = 68.49f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 48, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 48", Price = 18.6f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 49, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 49", Price = 68.18f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 50, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 50", Price = 85.93f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 51, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 51", Price = 6.69f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 52, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 52", Price = 87.05f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 53, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 53", Price = 82.94f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 54, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 54", Price = 88.8f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 55, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 55", Price = 80.77f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 56, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 56", Price = 93.16f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 57, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 57", Price = 38.42f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 58, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 58", Price = 79.91f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 59, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 59", Price = 71.87f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 60, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 60", Price = 14.55f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 61, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 61", Price = 95.16f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 62, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 62", Price = 31.71f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 63, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 63", Price = 98.56f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 64, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 64", Price = 1.77f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 65, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 65", Price = 59.18f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 66, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 66", Price = 1.49f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 67, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 67", Price = 36.14f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 68, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 68", Price = 80.12f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 69, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 69", Price = 58.99f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 70, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 70", Price = 1.31f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 71, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 71", Price = 9.85f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 72, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 72", Price = 2.72f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 73, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 73", Price = 12.08f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 74, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 74", Price = 56.42f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 75, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 75", Price = 46.53f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 76, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 76", Price = 36.26f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 77, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 77", Price = 49.04f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 78, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 78", Price = 39.56f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 79, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 79", Price = 20.08f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 80, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 80", Price = 61.85f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 81, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 81", Price = 72.39f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 82, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 82", Price = 3.2f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 83, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 83", Price = 54.2f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 84, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 84", Price = 68.67f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 85, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 85", Price = 28.68f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 86, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 86", Price = 17.15f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 87, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 87", Price = 57.47f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 88, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 88", Price = 51.78f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 89, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 89", Price = 20.02f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 90, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 90", Price = 10.88f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 91, CategoryId = 2, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 91", Price = 94f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 92, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 92", Price = 55.98f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 93, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 93", Price = 32.69f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 94, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 94", Price = 34.11f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 95, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 95", Price = 12.27f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 96, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 96", Price = 93.1f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 97, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 97", Price = 26.02f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 98, CategoryId = 1, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 98", Price = 51.22f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) },
                        new { Id = 99, CategoryId = 3, Created_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local), Name = "Product 99", Price = 42.07f, UnitsInStock = 50, Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 732, DateTimeKind.Local) }
                    );
                });

            modelBuilder.Entity("ELEKS_REST.Model.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created_At")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("FirstName")
                        .HasColumnType("varchar(40)");

                    b.Property<string>("LastName")
                        .HasColumnType("varchar(40)");

                    b.Property<string>("MidName")
                        .HasColumnType("varchar(40)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("varchar(1000)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("varchar(13)");

                    b.Property<string>("Role");

                    b.Property<string>("Token")
                        .HasColumnType("varchar(1000)");

                    b.Property<DateTime>("Updated_At")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new { Id = 1, Created_At = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Email = "vod.ukr@gmail.com", FirstName = "Vladyslav", LastName = "Dudka", MidName = "Oleksandrovich", Password = "test123", PhoneNumber = "+380507377789", Role = "admin", Token = "", Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 729, DateTimeKind.Local) },
                        new { Id = 2, Created_At = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Email = "example@mail.com", FirstName = "Igor", LastName = "Taranov", MidName = "Oleksandrovich", Password = "cust123456", PhoneNumber = "+380739999999", Role = "customer", Token = "", Updated_At = new DateTime(2018, 11, 13, 15, 4, 5, 731, DateTimeKind.Local) }
                    );
                });

            modelBuilder.Entity("ELEKS_REST.Model.UserProduct", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created_At")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<int?>("ProductId");

                    b.Property<int>("Quantity");

                    b.Property<DateTime>("Updated_At")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("ProductId");

                    b.HasIndex("UserId");

                    b.ToTable("UserProducts");
                });

            modelBuilder.Entity("ELEKS_REST.Model.Product", b =>
                {
                    b.HasOne("ELEKS_REST.Model.Category", "Category")
                        .WithMany("Products")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ELEKS_REST.Model.UserProduct", b =>
                {
                    b.HasOne("ELEKS_REST.Model.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId");

                    b.HasOne("ELEKS_REST.Model.User", "User")
                        .WithMany("Basket")
                        .HasForeignKey("UserId");
                });
#pragma warning restore 612, 618
        }
    }
}
