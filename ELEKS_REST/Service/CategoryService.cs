﻿using ELEKS_REST.Context;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using ELEKS_REST.Repository;
using ELEKS_REST.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<Product> _productRepository;

        public CategoryService(IRepository<Category> _categoryRepository, IRepository<Product> _productRepository)
        {
            this._categoryRepository = _categoryRepository;
            this._productRepository = _productRepository;
        }

        private ApiResponse Validate(CategoryDto categoryDto)
        {
            List<string> errors = new List<string>();

            if (categoryDto.Name.Length <= 3 || categoryDto.Name.Length >= 40)
                errors.Add("Category must be between 3 and 40.");

            if (errors.Count > 0)
                return new ApiResponse(ResponseStatus.Failed, errors);
            else
                return null;
        }

        public ApiResponse<List<CategoryDto>> GetAll(bool joinProducts = false)
        {
            try
            {
                List<CategoryDto> list = new List<CategoryDto>();
                List<Category> categories = new List<Category>();


                if (joinProducts)
                    categories = _categoryRepository.Table.Expand(c => c.Products).OrderByDescending(c => c.Id).ToList();
                else
                    categories = _categoryRepository.Table.OrderByDescending(c => c.Id).ToList();

                foreach (var item in categories)
                {
                    CategoryDto categoryDto = new CategoryDto(item);

                    if (joinProducts) {
                        List<ProductDto> temp = new List<ProductDto>();

                        foreach (var prod in item.Products)
                            temp.Add(new ProductDto(prod));

                        categoryDto.Products = temp;
                    } 

                    list.Add(categoryDto);
                }

                return new ApiResponse<List<CategoryDto>>(ResponseStatus.Success, "Categories received", list);
            }
            catch (Exception ex)
            {
                return new ApiResponse<List<CategoryDto>>(ResponseStatus.Error, ex.Message, null);
            }
        }

        public ApiResponse<CategoryDto> GetById(int id)
        {
            try
            {
                Category cat = _categoryRepository.Get(id);
                if (cat != null)
                {
                    CategoryDto dto = new CategoryDto(cat);

                    return new ApiResponse<CategoryDto>(ResponseStatus.Success, $"Category by id: {id}", dto);
                }
                else
                    return new ApiResponse<CategoryDto>(ResponseStatus.Failed, "Category not found", null);
            }
            catch (Exception ex)
            {
                return new ApiResponse<CategoryDto>(ResponseStatus.Error, ex.Message, null);
            }
        }

        public ApiResponse Insert(CategoryDto categoryDto)
        {
            ApiResponse validate = this.Validate(categoryDto);
            if (validate != null)
                return validate;

            try
            {
                Category category = new Category()
                {
                    Name = categoryDto.Name,
                };

                _categoryRepository.Add(category);

                return new ApiResponse(ResponseStatus.Success, "Category has been added");
            }
            catch (Exception ex)
            {
                return new ApiResponse(ResponseStatus.Error, ex.Message);
            }
        }

        public ApiResponse Update(CategoryDto categoryDto)
        {
            ApiResponse validate = this.Validate(categoryDto);
            if (validate != null)
                return validate;

            try
            {
                Category category = new Category()
                {
                    Id = categoryDto.Id,
                    Name = categoryDto.Name,
                };

                _categoryRepository.Update(category);

                return new ApiResponse(ResponseStatus.Success, "Category has been updated");
            }
            catch (Exception ex)
            {
                return new ApiResponse(ResponseStatus.Error, ex.Message);
            }
        }

        public ApiResponse Remove(CategoryDto categoryDto)
        {
            try
            {
                Category category = _categoryRepository.Get(categoryDto.Id);

                _categoryRepository.Remove(category);

                return new ApiResponse(ResponseStatus.Success, "Category has been deleted");
            }
            catch (Exception ex)
            {
                return new ApiResponse(ResponseStatus.Error, ex.Message);
            }
        }

        public ApiResponse RemoveById(int id)
        {
            try
            {
                Category category = _categoryRepository.Get(id);

                _categoryRepository.Remove(category);

                return new ApiResponse(ResponseStatus.Success, "Category has been deleted");
            }
            catch (Exception ex)
            {
                return new ApiResponse(ResponseStatus.Error, ex.Message);
            }
        }
    }
}
