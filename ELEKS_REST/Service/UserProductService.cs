﻿using ELEKS_REST.Context;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using ELEKS_REST.Repository;
using ELEKS_REST.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Service
{
    public class UserProductService : IUserProductService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserProduct> _userProductRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Category> _categoryRepository;

        public UserProductService(IRepository<User> _userRepository, IRepository<UserProduct> _userProductRepository, IRepository<Category> _categoryRepository, IRepository<Product> _productRepository)
        {
            this._userRepository = _userRepository;
            this._userProductRepository = _userProductRepository;
            this._productRepository = _productRepository;
            this._categoryRepository = _categoryRepository;
        }

        private ApiResponse Validate(CategoryDto categoryDto)
        {
            List<string> errors = new List<string>();

            if (categoryDto.Name.Length <= 3 || categoryDto.Name.Length >= 40)
                errors.Add("Category must be between 3 and 40.");

            if (errors.Count > 0)
                return new ApiResponse(ResponseStatus.Failed, errors);
            else
                return null;
        }

        public ApiResponse<BasketDto> GetByUserEmail(string email)
        {
            try
            {
                User user = _userRepository.Table.FirstOrDefault(u => u.Email == email);
                if (user != null)
                 {
                     user.Basket = _userProductRepository.Table.Expand(up => up.Product).Expand(up => up.Product.Category).OrderByDescending(up => up.Created_At).Where(up => up.User.Id == user.Id).ToList();

                     List<ProductDto> products = new List<ProductDto>();

                     foreach (var item in user.Basket)
                     {
                         ProductDto temp = new ProductDto(item.Product)
                         {
                             Quantity = item.Quantity
                         };
                         products.Add(temp);
                     }

                     BasketDto dto = new BasketDto()
                     {
                         Email = user.Email,
                         TotalPrice = user.BasketSum,
                         Products = products,
                     };

                     return new ApiResponse<BasketDto>(ResponseStatus.Success, "Basket received", dto);
                 }
                 else
                     return new ApiResponse<BasketDto>(ResponseStatus.Failed, "User not found", null);
            }
            catch (Exception ex)
            {
                return new ApiResponse<BasketDto>(ResponseStatus.Error, ex.Message, null);
            }
        }

        public ApiResponse Insert(string email, int prodId, int quantity = 1)
        {
            Product prod = _productRepository.Get(prodId);
            if (prod != null) {
                try
                {
                    User user = _userRepository.FirstOrDefault(u => u.Email == email);
                    UserProduct userProduct = _userProductRepository.Table.FirstOrDefault(up => up.User.Id == user.Id && up.Product.Id == prodId);

                    if (userProduct == null)
                    {
                        userProduct = new UserProduct()
                        {
                            User = user,
                            Product = prod,
                        };
                        _userProductRepository.Add(userProduct);
                    }
                    else
                    {
                        userProduct.Quantity += quantity;
                        _userProductRepository.Update(userProduct);
                    }

                    return new ApiResponse(ResponseStatus.Success, "Product has been added to basket");
                }
                catch (Exception ex)
                {
                    return new ApiResponse(ResponseStatus.Error, ex.Message);
                }
            } else
                return new ApiResponse(ResponseStatus.Failed, "Product not found");
        }

        public ApiResponse Remove(string email, int prodId, int quantity = 1)
        {
            Product prod = _productRepository.Get(prodId);
            if (prod != null)
            {
                try
                {
                    User user = _userRepository.FirstOrDefault(u => u.Email == email);
                    UserProduct userProduct = _userProductRepository.Table.FirstOrDefault(up => up.User.Id == user.Id && up.Product.Id == prodId);

                    if (userProduct != null)
                    {
                        if (userProduct.Quantity - quantity <= 0)
                            _userProductRepository.Remove(userProduct);
                        else
                        {
                            userProduct.Quantity -= quantity;
                            _userProductRepository.Update(userProduct);
                        }
                    }

                    return new ApiResponse(ResponseStatus.Success, "Product has been deleted from basket");
                }
                catch (Exception ex)
                {
                    return new ApiResponse(ResponseStatus.Error, ex.Message);
                }
            }
            else
                return new ApiResponse(ResponseStatus.Failed, "Product not found");
        }
    }
}
