﻿using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Service.Interface
{
    public interface IProductService
    {
        ApiResponse<List<ProductDto>> GetAll();
        ApiResponse<List<ProductDto>> GetAll(int page, string filterBy = null, int categoryId = 0);
        ApiResponse<ProductDto> GetById(int id);
        ApiResponse Insert(ProductDto productDto);
        ApiResponse Update(ProductDto productDto);
        ApiResponse Remove(ProductDto productDto);
        ApiResponse RemoveById(int id);
    }
}
