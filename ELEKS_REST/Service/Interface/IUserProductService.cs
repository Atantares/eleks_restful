﻿using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Service.Interface
{
    public interface IUserProductService
    {
        ApiResponse<BasketDto> GetByUserEmail(string email);
        ApiResponse Insert(string email, int prodId, int quantity = 1);
        ApiResponse Remove(string email, int prodId, int quantity = 1);
    }
}
