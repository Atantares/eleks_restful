﻿using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ELEKS_REST.Service.Interface
{
    public interface IUserService
    {
        ApiResponse<UserDto> Authenticate(UserDto userDto);
    }
}
