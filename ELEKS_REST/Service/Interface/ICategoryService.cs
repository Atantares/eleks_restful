﻿using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Service.Interface
{
    public interface ICategoryService
    {
        ApiResponse<List<CategoryDto>> GetAll(bool joinProducts = false);
        ApiResponse<CategoryDto> GetById(int id);
        ApiResponse Insert(CategoryDto categoryDto);
        ApiResponse Update(CategoryDto categoryDto);
        ApiResponse Remove(CategoryDto categoryDto);
        ApiResponse RemoveById(int id);
    }
}
