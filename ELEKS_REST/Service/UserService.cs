﻿using ELEKS_REST.Context;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using ELEKS_REST.Repository;
using ELEKS_REST.Service.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ELEKS_REST.Service
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;

        public UserService(IRepository<User> userRepository)
        {
            this._userRepository = userRepository;
        }

        public ApiResponse<UserDto> Authenticate(UserDto userDto)
        {
            User user = _userRepository.FirstOrDefault(x => x.Email == userDto.Email && x.Password == userDto.Password);
            if (user == null)
                return new ApiResponse<UserDto>(ResponseStatus.Failed, "Email or/and password is incorrect", null);

            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role)
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            var now = DateTime.UtcNow;

            IConfiguration config = new ConfigurationBuilder()
            .AddJsonFile("general.json", true, true)
            .Build();

            var jwt = new JwtSecurityToken(
                    issuer: config["AuthOptions:ISSUER"],
                    audience: config["AuthOptions:AUDIENCE"],
                    notBefore: now,
                    claims: claimsIdentity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(int.Parse(config["AuthOptions:LIFETIME"]))),
                    signingCredentials: new SigningCredentials(Auth.GetSymmetricSecurityKey(config["AuthOptions:KEY"]), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            user.Token = encodedJwt;
            _userRepository.Update(user);

            userDto = new UserDto()
            {
                Id = user.Id,
                Email = user.Email,
                FullName = user.FullName,
                PhoneNumber = user.PhoneNumber,
                Role = user.Role,
                Token = user.Token,
            };

            return new ApiResponse<UserDto>(ResponseStatus.Success, "Authentication successful", userDto);
        }
    }
}
