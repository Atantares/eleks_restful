﻿using ELEKS_REST.Context;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using ELEKS_REST.Repository;
using ELEKS_REST.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Service
{
   
    public class ProductService : IProductService
    {
        private readonly IRepository<Category> _catRepository;
        private readonly IRepository<Product> _prodRepository;

        public ProductService(
            IRepository<Product> prodRepository, 
            IRepository<Category> catRepository)
        {
            this._prodRepository = prodRepository;
            this._catRepository = catRepository;
        }

        private ApiResponse Validate(ProductDto productDto)
        {
            List<string> errors = new List<string>();

            if (productDto.Name.Length <= 4 || productDto.Name.Length >= 150)
                errors.Add("Name must be between 4 and 150.");
            else if(productDto.Price <= 0)
                errors.Add("Price must not be negative or equal to zero");

            if (errors.Count > 0)
                return new ApiResponse(ResponseStatus.Failed, errors);
            else
                return null;
        }

        public ApiResponse<List<ProductDto>> GetAll()
        {
            try
            {
                List<ProductDto> list = new List<ProductDto>();

                foreach (var item in _prodRepository.Table.Expand(p => p.Category))
                {
                    ProductDto temp = new ProductDto(item);
                    list.Add(temp);
                }

                return new ApiResponse<List<ProductDto>>(ResponseStatus.Success, "Products received", list);
            }
            catch (Exception ex)
            {
                return new ApiResponse<List<ProductDto>>(ResponseStatus.Error, ex.Message, null);
            }
        }

        public ApiResponse<List<ProductDto>> GetAll(int page, string filterBy = null, int categoryId = 0)
        {
            try
            {
                if (page <= 0)
                    page = 1;

                List<ProductDto> list = new List<ProductDto>();
                List<Product> prodList = new List<Product>();

                var prodTable = _prodRepository.Table.Expand(p => p.Category);


                if (filterBy == "nameAsc" || filterBy == "name")
                    prodTable.OrderBy(p => p.Name);
                else if (filterBy == "nameDesc")
                    prodTable.OrderByDescending(p => p.Name);
                else if (filterBy == "priceAsc" || filterBy == "price")
                    prodTable.OrderBy(p => p.Price);
                else if (filterBy == "priceDesc")
                    prodTable.OrderByDescending(p => p.Price);

                if (categoryId > 0)
                    prodList = prodTable.Where(p => p.Category.Id == categoryId).ToList();
                else
                    prodList = prodTable.ToList();


                foreach (var item in prodList)
                {
                    ProductDto temp = new ProductDto(item);
                    list.Add(temp);
                }

                int pageSize = PagingParameters.maxPageSize;
                int totalCount = list.Count;
                int TotalPages = (int)Math.Ceiling(totalCount / (double)pageSize);

                PagingParameters pagingParameters = new PagingParameters(TotalPages, page);

                list = list.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                return new ApiResponse<List<ProductDto>>(ResponseStatus.Success, "Products received", list, pagingParameters);
            }
            catch (Exception ex)
            {
                return new ApiResponse<List<ProductDto>>(ResponseStatus.Error, ex.Message, null);
            }
        }



        public ApiResponse<ProductDto> GetById(int id)
        {
            try
            {
                Product prod = _prodRepository.Table.Expand(p => p.Category).FirstOrDefault(p => p.Id == id);
                if (prod != null) {
                    ProductDto dto = new ProductDto(prod);

                    return new ApiResponse<ProductDto>(ResponseStatus.Success, "Product by id: " + id, dto);
                }
                else
                    return new ApiResponse<ProductDto>(ResponseStatus.Failed, "Product not found", null);
            }
            catch (Exception ex)
            {
                return new ApiResponse<ProductDto>(ResponseStatus.Error, ex.Message, null);
            }
        }

        public ApiResponse Insert(ProductDto productDto)
        {
            ApiResponse validate = this.Validate(productDto);
            if (validate != null)
                return validate;

            Category category = _catRepository.Get(productDto.Category_Id);

            if (category != null) {
                try
                {
                    Product product = new Product()
                    {
                        Name = productDto.Name,
                        Price = productDto.Price,
                        UnitsInStock = productDto.UnitsInStock,
                        Category = category,
                    };

                    _prodRepository.Add(product);

                    return new ApiResponse(ResponseStatus.Success, "Product has been added");
                }
                catch(Exception ex)
                {
                    return new ApiResponse(ResponseStatus.Error, ex.Message);
                }
            }
            else
                return new ApiResponse(ResponseStatus.Failed, "Category not found");
        }

        public ApiResponse Update(ProductDto productDto)
        {
            ApiResponse validate = this.Validate(productDto);
            if (validate != null)
                return validate;

            Category category = _catRepository.Get(productDto.Category_Id);

            if (category != null)
            {
                try
                {
                    Product product = new Product()
                    {
                        Id = productDto.Id,
                        Name = productDto.Name,
                        Price = productDto.Price,
                        UnitsInStock = productDto.UnitsInStock,
                        Category = category,
                    };

                    _prodRepository.Update(product);

                    return new ApiResponse(ResponseStatus.Success, "Product has been updated");
                }
                catch (Exception ex)
                {
                    return new ApiResponse(ResponseStatus.Error, ex.Message);
                }
            }
            else
                return new ApiResponse(ResponseStatus.Failed, "Category not found");
        }

        public ApiResponse Remove(ProductDto productDto)
        {
            try
            {
                Product product = _prodRepository.Get(productDto.Id);

                _prodRepository.Remove(product);

                return new ApiResponse(ResponseStatus.Success, "Product has been deleted");
            }
            catch (Exception ex)
            {
                return new ApiResponse(ResponseStatus.Error, ex.Message);
            }
        }

        public ApiResponse RemoveById(int id)
        {
            try
            {
                Product product = _prodRepository.Get(id);

                _prodRepository.Remove(product);

                return new ApiResponse(ResponseStatus.Success, "Product has been deleted");
            }
            catch (Exception ex)
            {
                return new ApiResponse(ResponseStatus.Error, ex.Message);
            }
        }
    }
}
