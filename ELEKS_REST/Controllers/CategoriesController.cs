﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ELEKS_REST.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private ICategoryService _categoryService;

        public CategoriesController(ICategoryService _categoryService)
        {
            this._categoryService = _categoryService;
        }

        // GET: api/Categories
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get(bool joinProducts = false)
        {
            ApiResponse<List<CategoryDto>> response = _categoryService.GetAll(joinProducts);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.ResponseObj.Count == 0)
                return NoContent();

            return Ok(response);
        }

        // GET: api/Categories/5
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetCategory")]
        public IActionResult Get(int id)
        {
            ApiResponse<CategoryDto> response = _categoryService.GetById(id);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.ResponseObj == null)
                return NotFound();

            return Ok(response);
        }

        // POST: api/Categories
        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Post([FromBody] CategoryDto categoryDto)
        {
            ApiResponse response = _categoryService.Insert(categoryDto);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }

        // PUT: api/Categories/5
        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] CategoryDto categoryDto)
        {
            categoryDto.Id = id;
            ApiResponse response = _categoryService.Update(categoryDto);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }

        // DELETE: api/ApiWithActions/5
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            ApiResponse response = _categoryService.RemoveById(id);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }
    }
}
