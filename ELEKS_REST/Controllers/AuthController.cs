﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ELEKS_REST.Context;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using ELEKS_REST.Repository;
using ELEKS_REST.Service;
using ELEKS_REST.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace ELEKS_REST.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IUserService _userService;

        public AuthController(IUserService userRep)
        {
            _userService = userRep;
        }

        [HttpPost]
        public IActionResult Token([FromBody] UserDto userDto)
        {
            ApiResponse response = _userService.Authenticate(userDto);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }
    }
}