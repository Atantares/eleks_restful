﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ELEKS_REST.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        private IUserProductService _userProductService;

        public BasketController(IUserProductService _userProductService)
        {
            this._userProductService = _userProductService;
        }

        // GET: api/Basket
        [HttpGet]
        public IActionResult Get()
        {
            ApiResponse<BasketDto> response = _userProductService.GetByUserEmail(User.Identity.Name);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            if (response.ResponseObj.Products.Count == 0)
                return NoContent();

            return Ok(response);
        }

        // POST: api/Basket
        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] int id, int quantity = 1)
        {
            ApiResponse response = _userProductService.Insert(User.Identity.Name, id, quantity);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute]int id, int quantity = 1)
        {
            ApiResponse response = _userProductService.Remove(User.Identity.Name, id, quantity);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }
    }
}
