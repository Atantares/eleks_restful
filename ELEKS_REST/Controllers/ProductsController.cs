﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ELEKS_REST.Dto;
using ELEKS_REST.Helpers;
using ELEKS_REST.Model;
using ELEKS_REST.Service;
using ELEKS_REST.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ELEKS_REST.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IProductService _productService;

        public ProductsController(IProductService _productService)
        {
            this._productService = _productService;
        }

        // GET: api/Products
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            ApiResponse<List<ProductDto>> response = _productService.GetAll(1);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.ResponseObj.Count == 0)
                return NoContent();

            return Ok(response);
        }

        // GET: api/Products/page/1/filterBy/nameDesc
        [AllowAnonymous]
        [HttpGet]
        [Route("page/{page}/")]
        public IActionResult GetByFilter([FromQuery] int page, string filterBy, int categoryId)
        {
            ApiResponse<List<ProductDto>> response = _productService.GetAll(page, filterBy, categoryId);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.ResponseObj.Count == 0)
                return NoContent();

            return Ok(response);
        }

        // GET: api/Products/5
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetProduct")]
        public IActionResult Get(int id)
        {
            ApiResponse<ProductDto> response = _productService.GetById(id);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.ResponseObj == null)
                return NotFound();

            return Ok(response);
        }

        // POST: api/Products
        [Authorize(Roles = "admin")]
        [HttpPost]
        public IActionResult Post([FromBody] ProductDto productDto)
        {
            ApiResponse response = _productService.Insert(productDto);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }

        // PUT: api/Products/5
        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProductDto productDto)
        {
            productDto.Id = id;
            ApiResponse response = _productService.Update(productDto);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }

        // DELETE: api/Products/5
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            ApiResponse response = _productService.RemoveById(id);

            if (response.Status == ResponseStatus.Error)
                return StatusCode(500);

            if (response.Status == ResponseStatus.Failed)
                return BadRequest(response);

            return Ok(response);
        }
    }
}
