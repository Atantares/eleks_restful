﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Model
{
    public class Product
    {
        public int Id { get; set; }
        [Required, Column(TypeName = "varchar(255)")]
        public string Name { get; set; }
        public float Price { get; set; } = 0;
        public int UnitsInStock { get; set; } = 0;
        [Required]
        public Category Category { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime Created_At { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Updated_At { get; set; } = DateTime.Now;
    }
}
