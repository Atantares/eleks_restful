﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Model
{
    public class User
    {
        public int Id { get; set; }
        [Required, Column(TypeName = "varchar(50)")]
        public string Email { get; set; }
        [Required, Column(TypeName = "varchar(1000)")]
        public string Password { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string MidName { get; set; }
        [Column(TypeName = "varchar(13)")]
        public string PhoneNumber { get; set; }
        public string Role { get; set; } = "customer";
        [Column(TypeName = "varchar(1000)")]
        public string Token { get; set; } = String.Empty;

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime Created_At { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Updated_At { get; set; } = DateTime.Now;

        public ICollection<UserProduct> Basket { get; set; }

        public User()
        {
            Basket = new List<UserProduct>();
        }

        [NotMapped]
        public float BasketSum
        {
            get
            {
                return Basket.Sum(c => c.Product.Price);
            }
        }

        [NotMapped]
        public string FullName
        {
            get
            {
                return LastName + " " + FirstName + " " + MidName;
            }
        }
    }
}
