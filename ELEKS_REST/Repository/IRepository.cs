﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ELEKS_REST.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Table{ get; }
        IQueryable<TEntity> TableUntracked { get; }
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        TEntity Add(TEntity entity);
        void Update(TEntity entity);

        void Remove(TEntity entity);

        IQueryable<TEntity> Expand(IQueryable<TEntity> query, string path);
        IQueryable<TEntity> Expand<TProperty>(IQueryable<TEntity> query, Expression<Func<TEntity, TProperty>> path);
    }
}
