﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Helpers
{
    public class PagingParameters
    {
        [JsonProperty]
        public const int maxPageSize = 20;

        public int TotalPages { get; set; }

        public int CurrentPage { get; set; } = 1;

        public PagingParameters(int TotalPages, int CurrentPage)
        {
            this.TotalPages = TotalPages;
            this.CurrentPage = CurrentPage;
        }
    }
}
