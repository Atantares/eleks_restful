﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace ELEKS_REST.Helpers
{
    public enum ResponseStatus
    {
        Success,
        Failed,
        Error
    }

    public class ApiResponse
    {
        public const string ModelBindingErrorMessage = "Invalid parameters.";

        public ApiResponse()
        {
        }

        public ApiResponse(string message)
        {
            Message = new List<string>() { message };
        }

        public ApiResponse(ResponseStatus status, string message)
        {
            Status = status;
            Message = new List<string>() { message };
        }

        public ApiResponse(ResponseStatus status, List<string> messages)
        {
            Status = status;
            Message = messages;
        }

        [JsonIgnore]
        public ResponseStatus Status { get; set; }
        public List<string> Message { get; set; }

    }

    public class ApiResponse<T> : ApiResponse
    {

        public ApiResponse(ResponseStatus status, string message, T responseObj) : base(status, message)
        {
            ResponseObj = responseObj;
        }

        public ApiResponse(ResponseStatus status, string message, T responseObj, PagingParameters pagingParameters) : base(status, message)
        {
            ResponseObj = responseObj;
            PagingParameters = pagingParameters;
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public T ResponseObj { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PagingParameters PagingParameters { get; set; }

    }
}
