﻿using ELEKS_REST.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Dto
{
    public class BasketDto
    {
        public string Email { get; set; }
        public float TotalPrice { get; set; }
        public List<ProductDto> Products { get; set; }
    }
}
