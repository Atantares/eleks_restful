﻿using ELEKS_REST.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Dto
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ProductDto> Products { get; set; }

        public CategoryDto() {
            Products = new List<ProductDto>();
        }

        public CategoryDto(Category cat)
        {
            Id = cat.Id;
            Name = cat.Name;
        }
    }
}
