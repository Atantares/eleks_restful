﻿using ELEKS_REST.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELEKS_REST.Dto
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public int UnitsInStock { get; set; }
        public int Category_Id { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int Quantity { get; set; }

        public ProductDto() { }

        public ProductDto(Product prod)
        {
            Id = prod.Id;
            Name = prod.Name;
            Price = prod.Price;
            UnitsInStock = prod.UnitsInStock;
            Category_Id = prod.Category.Id;
        }
    }
}
