# Shop Web API (ELEKS Task)

![](https://i.imgur.com/zWz98iP.png) Based on **ASP.NET Core 2.1**
Demo : [eleks-rest.tk](https://eleks-rest.tk "eleks-rest.tk")

## Requirements
- Postresql version `>=9.x`
- .NET Core version `2.1`
- Entity Framework Core `2.1.x`
- Npgsql `2.1.x` (Driver for *Postresql*)

## Install
1. Clone this project
2. Rename file `general.json.example` to `general.json`
3. Configure options in  `general.json`(Auth and DB connection data)
4. Run command `Update-Database` in *Package Manager Console*  for create tables in DB

## List of Entities(Models) in DB
- User
- Product
- Category
- UserProduct (Basket with products for user)

## Authorization in API
Route:

     https://eleks-rest.tk/api/auth

Request Body example:
```json
{
	"Email": "vod.ukr@gmail.com",
	"Password": "password123",
}
```
Your response:
```json
{
    "responseObj": {
        "token": "your_access_token_here"
    },
    "message": [
        "Authentication successful"
    ]
}
```
Take this access token and insert to HTTP Header(every request).
Key: `Authorization`
Value: `Bearer your_access_token`
## List of Routes
GetAll Products with pagination, filter by name/price, by category id:


    https://eleks-rest.tk/api/products/page/number_of_page/
    https://eleks-rest.tk/api/products/page/number_of_page/?filterBy=nameDesc|nameAsc|priceDesc|priceAsc
    https://eleks-rest.tk/api/products/page/number_of_page/?filterBy=nameDesc|nameAsc|priceDesc|priceAsc&categoryId=1

GetAll Categories (default without products):


    https://eleks-rest.tk/api/categories
    https://eleks-rest.tk/api/categories?joinProducts=true

Get Product by id:

    https://eleks-rest.tk/api/products/{id}

Get Category by id:


    https://eleks-rest.tk/api/categories/{id}

Add new Product (if role = admin):

    https://eleks-rest.tk/api/products/
Method: POST
Request Body example:
```json
{
    "name": "product name",
    "price": 555,
    "unitsInStock": 15,
    "category_id": 2
}
```
Add new Category (if role = admin):

    https://eleks-rest.tk/api/categories/
Method: POST
Request Body example:
```json
{
    "name": "category name"
}
```
Update Product (if role = admin):

    https://eleks-rest.tk/api/products/
Method: PUT
Request Body example:
```json
{
    "name": "product name",
    "price": 555,
    "unitsInStock": 15,
    "category_id": 2
}
```
Update Category (if role = admin):

    https://eleks-rest.tk/api/categories/
Method: PUT
Request Body example:
```json
{
    "name": "category name"
}
```
Delete Product (if role = admin):
Method: DELETE

    https://eleks-rest.tk/api/products/{id}

Delete Category (if role = admin):
Method: DELETE

    https://eleks-rest.tk/api/categories/{id}

Basket (if user authenticated)


    GET:
    http://eleks-rest.tk/api/basket
    
    POST and DELETE (default quantity 1):
    http://eleks-rest.tk/api/basket/{id}
    http://eleks-rest.tk/api/basket/{id}?quantity=1